/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Application/Service/Registry.h>
#include <GitComplex/Execution/Service/Std/Concrete/Registry.h>
#include <GitComplex/Shell/Executor/Service/Std/Concrete/Registry.h>
#include <GitComplex/Repository/Service/Std/Concrete/Registry.h>
#include <GitComplex/Repository/Service/Qt/Concrete/Registry.h>
#include <GitComplex/Execution/Entity/Std/Concrete/Executor.h>
#include <GitComplex/Settings/Utility.h>

/*!
 * \file src/GitComplex/Application/Service/Registry.cpp
 * \brief src/GitComplex/Application/Service/Registry.cpp
 */

namespace GitComplex::Application::Service
{

using ConcreteTaskExecutorService  = Execution::Service::Std::Concrete::Registry;
using ConcreteShellExecutorService = Shell::Executor::Service::Std::Concrete::Registry;
using ConcreteRepositoryStdService = Repository::Service::Std::Concrete::Registry;
using ConcreteRepositoryQtService  = Repository::Service::Qt::Concrete::Registry;

Registry::Registry()
    : m_task{otn::itself_type<ConcreteTaskExecutorService>},
      m_shell{otn::itself_type<ConcreteShellExecutorService>},
      m_std_repository{otn::itself_type<ConcreteRepositoryStdService>,
                       (*m_task).executor(),
                       (*m_shell).executor()},
      m_repository{otn::itself_type<ConcreteRepositoryQtService>,
                   (*m_std_repository).idProvider(),
                   (*m_std_repository).structureScanner(),
                   (*m_std_repository).statusProvider(),
                   (*m_std_repository).commandExecutor()}
{}

void Registry::save(Settings::UserStorage& storage) const
{
    Settings::save_pmr(*m_repository, storage, "Repository");
}

void Registry::load(Settings::UserStorage& storage)
{
    Settings::load_pmr(*m_repository, storage, "Repository");
}

} // namespace GitComplex::Application::Service
