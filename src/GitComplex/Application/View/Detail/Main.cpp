﻿/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "ui_Main.h"

#include <GitComplex/Application/View/Detail/Main.h>

/*!
 * \file src/GitComplex/Application/View/Detail/Main.cpp
 * \brief src/GitComplex/Application/View/Detail/Main.cpp
 */

namespace GitComplex::Application::View::Detail
{

Main::Main(QWidget* parent, ::Qt::WindowFlags flags)
    : View::Main{parent, flags},
      ui{new Ui::Main()}
{
    ui->setupUi(this);

    setupMenuBar();
}

Main::~Main()
{
    delete ui;
}

void Main::setupMenuBar()
{
    ui->exitAction->setIcon(
        QIcon::fromTheme(QStringLiteral("application-exit")));

    ui->exitSeparator->setSeparator(true);

    connect(ui->exitAction, &QAction::triggered, this, &QMainWindow::close);
}

void Main::save(Settings::RepositoryStorage& storage) const
{
    storage.setValue("Geometry", saveGeometry());
    storage.setValue("State",    saveState());
}

void Main::load(Settings::RepositoryStorage& storage)
{
    restoreGeometry(storage.value("Geometry").toByteArray());
    restoreState(storage.value("State").toByteArray());
}

void Main::emplaceWidget(Slot::BatchWorkspace batch_workspace)
{
    m_batch_workspace = std::move(batch_workspace);
    setCentralWidget(m_batch_workspace);
}

void Main::emplaceActions(const RootListEditorActions& actions)
{
    m_root_list_editor_actions = actions;
    ui->menuFile->insertAction(ui->exitSeparator,
                               m_root_list_editor_actions.removeRepositories);
    ui->menuFile->insertAction(m_root_list_editor_actions.removeRepositories,
                               m_root_list_editor_actions.addRepository);
}

} // namespace GitComplex::Application::View::Detail
