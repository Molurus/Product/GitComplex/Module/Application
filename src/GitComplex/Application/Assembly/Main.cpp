/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Application/Assembly/Main.h>
#include <GitComplex/Application/View/Detail/Main.h>
#include <GitComplex/Repository/RootList/Entity/Concrete/Storage.h>
#include <GitComplex/Repository/Selector/View/Concrete/FileSystemSelector.h>
#include <GitComplex/Settings/Utility.h>

/*!
 * \file src/GitComplex/Application/Assembly/Main.cpp
 * \brief src/GitComplex/Application/Assembly/Main.cpp
 */

namespace GitComplex::Application::Assembly
{

using ConcreteRootListStorage = Repository::RootList::Entity::Concrete::Storage;
using Repository::Selector::View::Concrete::FileSystemSelector;

Main::Main()
    : m_root_list_storage{otn::itself_type<ConcreteRootListStorage>},
      m_batch_workspace_frame{m_service.repository().idProvider(),
                              m_service.repository().structureScanner(),
                              *m_root_list_storage,
                              m_root_list_storage,
                              m_service.repository().statusProvider(),
                              m_service.repository().toolLauncher(),
                              m_service.repository().commandExecutor()},
      m_root_list_editor{m_root_list_storage,
                         otn::slim::make_unique<FileSystemSelector>(),
                         m_batch_workspace_frame.makePathSelector(
                             UserInterface::Dialog::Confirmation::Close)},
      m_view{otn::itself_type<View::Detail::Main>}
{
    auto root_list_editor_actions = m_root_list_editor.actions();
    (*m_view).emplaceActions(root_list_editor_actions);
    (*m_batch_workspace_frame.view()).emplaceActions(root_list_editor_actions);

    (*m_view).emplaceWidget(
        View::Slot::BatchWorkspace{m_batch_workspace_frame.view()});
}

void Main::start()
{
    load(m_user_environment.userStorage());
    load(m_user_environment.repositoryStorage());

    loadWorkingDirRepository();

    show();
}

void Main::quit()
{
    // TODO: Stop m_service.m_repository.commandExecutor().

    save(m_user_environment.userStorage());
    save(m_user_environment.repositoryStorage());
}

void Main::save(Settings::UserStorage&& storage) const
{
    Settings::save_cnt(m_service, storage, "Service");
}

void Main::load(Settings::UserStorage&& storage)
{
    Settings::load_cnt(m_service, storage, "Service");
}

void Main::save(Settings::RepositoryStorage&& storage) const
{
    Settings::save_pmr(*m_root_list_storage, storage, "RootList");
    Settings::save_cnt(m_root_list_editor,      storage, "RootList");
    Settings::save_cnt(m_batch_workspace_frame, storage, "BatchWorkspace");
    Settings::save_pmr(*m_view, storage, "MainWindow");
}

void Main::load(Settings::RepositoryStorage&& storage)
{
    Settings::load_pmr(*m_root_list_storage, storage, "RootList");
    Settings::load_cnt(m_root_list_editor,      storage, "RootList");
    Settings::load_cnt(m_batch_workspace_frame, storage, "BatchWorkspace");
    Settings::load_pmr(*m_view, storage, "MainWindow");
}

void Main::show() const
{
    (*m_view).show();
}

void Main::loadWorkingDirRepository()
{
    auto working_dir_repository = m_user_environment.workingDirRepository();
    if (!working_dir_repository.isEmpty())
        (*m_root_list_storage).addRepositories(
            PathList{working_dir_repository});
}

} // namespace GitComplex::Application::Assembly
