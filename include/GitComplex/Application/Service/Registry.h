/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Execution/Service/Std/Registry.h>
#include <GitComplex/Shell/Executor/Service/Std/Registry.h>
#include <GitComplex/Repository/Service/Std/Registry.h>
#include <GitComplex/Repository/Service/Qt/Registry.h>
#include <GitComplex/Settings/Storage.h>

/*!
 * \file include/GitComplex/Application/Service/Registry.h
 * \brief include/GitComplex/Application/Service/Registry.h
 */

namespace GitComplex::Application::Service
{

/*!
 * \class Registry
 * \brief Registry.
 *
 * \headerfile GitComplex/Application/Service/Registry.h
 *
 * Registry.
 *
 * \note [[Viper::Interactor]] [[using Framework : Qt, Std]]
 */
class Registry final
{
private:
    using TaskExecutorService  = Execution::Service::Std::Registry;
    using ShellExecutorService = Shell::Executor::Service::Std::Registry;
    using RepositoryStdService = Repository::Service::Std::Registry;

public:
    using RepositoryService = Repository::Service::Qt::Registry;

    Registry();

    const RepositoryService& repository() const { return *m_repository; }

    void save(Settings::UserStorage& storage) const;
    void load(Settings::UserStorage& storage);

private:
    otn::slim::unique_single<TaskExecutorService>  m_task;
    otn::slim::unique_single<ShellExecutorService> m_shell;
    otn::slim::unique_single<RepositoryStdService> m_std_repository;
    otn::slim::unique_single<RepositoryService>    m_repository;
};

} // namespace GitComplex::Application::Service
