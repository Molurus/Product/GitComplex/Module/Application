/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Application/View/Main.h>
#include <GitComplex/Settings/Storable.h>

/*!
 * \file include/GitComplex/Application/View/Detail/Main.h
 * \brief include/GitComplex/Application/View/Detail/Main.h
 */

namespace Ui
{

class Main;

}

namespace GitComplex::Application::View::Detail
{

/*!
 * \class Main
 * \brief Main.
 *
 * \headerfile GitComplex/Application/View/Detail/Main.h
 *
 * Main.
 *
 * \note [[Viper::View]] [[Framework::Qt]]
 */
class Main final : public View::Main,
                   public Settings::RepositoryStorable
{
    Q_OBJECT

public:
    Main(QWidget* parent = nullptr, ::Qt::WindowFlags flags = {});
    ~Main() override;

private:
    using RootListEditorActions =
        Repository::RootList::Presenter::Actions::Editor;

    void save(Settings::RepositoryStorage& storage) const override;
    void load(Settings::RepositoryStorage& storage) override;

    void emplaceWidget(Slot::BatchWorkspace batch_workspace) override;

    void emplaceActions(const RootListEditorActions& actions) override;

    void setupMenuBar();

    // Ui.
    Ui::Main* ui;
    Widget::Holder<>      m_batch_workspace;
    RootListEditorActions m_root_list_editor_actions{};
};

} // namespace GitComplex::Application::View::Detail
