/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/UserSession/Environment.h>
#include <GitComplex/Repository/RootList/Assembly/Editor.h>
#include <GitComplex/BatchWorkspace/Assembly/Frame.h>
#include <GitComplex/Application/Service/Registry.h>
#include <GitComplex/Application/View/Main.h>

/*!
 * \file include/GitComplex/Application/Assembly/Main.h
 * \brief include/GitComplex/Application/Assembly/Main.h
 */

namespace GitComplex::Application::Assembly
{

/*!
 * \class Main
 * \brief Main.
 *
 * \headerfile GitComplex/Application/Assembly/Main.h
 *
 * Main.
 *
 * \note [[Viper::Assembly]] [[Framework::Qt]]
 */
class Main final
{
public:
    Main();

    void start();
    void quit();

private:
    void save(Settings::UserStorage&& storage) const;
    void load(Settings::UserStorage&& storage);
    void save(Settings::RepositoryStorage&& storage) const;
    void load(Settings::RepositoryStorage&& storage);

    void show() const;

    void loadWorkingDirRepository();

    using RootListStorage     = Repository::RootList::Entity::Storage;
    using RootListEditor      = Repository::RootList::Assembly::Editor;
    using BatchWorkspaceFrame = BatchWorkspace::Assembly::Frame;

    UserSession::Environment m_user_environment;

    Service::Registry m_service;

    otn::unique_single<RootListStorage> m_root_list_storage;
    BatchWorkspaceFrame m_batch_workspace_frame;
    RootListEditor      m_root_list_editor;

    View::Widget::Owner<View::Main> m_view;
};

} // namespace GitComplex::Application::Assembly
